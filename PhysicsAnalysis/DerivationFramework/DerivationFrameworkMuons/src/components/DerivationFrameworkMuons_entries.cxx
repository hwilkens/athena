#include "DerivationFrameworkMuons/dimuonTaggingTool.h"
#include "DerivationFrameworkMuons/isolationDecorator.h"
#include "DerivationFrameworkMuons/mcpDecorator.h"
#include "DerivationFrameworkMuons/MuonTPExtrapolationTool.h"
#include "DerivationFrameworkMuons/IDTrackCaloDepositsDecoratorTool.h"
#include "DerivationFrameworkMuons/MuonIDCovMatrixDecorator.h"
using namespace DerivationFramework;

DECLARE_COMPONENT( dimuonTaggingTool )
DECLARE_COMPONENT( isolationDecorator )
DECLARE_COMPONENT( mcpDecorator )
DECLARE_COMPONENT( MuonTPExtrapolationTool )
DECLARE_COMPONENT( IDTrackCaloDepositsDecoratorTool )
DECLARE_COMPONENT( MuonIDCovMatrixDecorator )

